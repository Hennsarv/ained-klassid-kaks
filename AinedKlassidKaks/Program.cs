﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AinedKlassidKaks
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\Data\Ained.txt";
            File.ReadAllLines(filename).ToList().ForEach(x => Console.WriteLine(x));
             
            var q = File.ReadAllLines(filename)
                .Select(x => x
                            .Split(',')
                            .Select(y => y.Trim())
                            .ToList())
                .Select(x => new Aine(x[0]).KlassiKoodid = x.Skip(1).ToList())
                .ToList()
                ;
            

            Aine.Ained.Values.ToList()
                .ForEach(x => Console.WriteLine(x));

        }
    }

    class Aine
    {
        public static Dictionary<string, Aine> Ained = new Dictionary<string, Aine>();
        public readonly string Nimetus;
        public List<string> KlassiKoodid = new List<string>();

        public Aine(string nimetus)
        {
            Nimetus = nimetus;
            Ained.Add(nimetus, this);
        }

        public override string ToString()
        {
            return "Ainet " + Nimetus + " õpetatakse " + string.Join(", ",KlassiKoodid) + " klassis";
        }
    }
}
